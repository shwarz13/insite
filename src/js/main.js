$(document).ready(function () {
  'use strict';
  if($('.page-index').length) {
    //popover for map's points
    $('.point').popover({
      content: function () {
        var elementId = $(this).attr("data-popover-content");
        return $(elementId).html();
      }
    });
    //point map projects
    $(".point").on('click', function () {
      $(this).toggleClass('show');
    });
    //vert_line slider
    $(".vert-line").css('height', `${document.documentElement.clientHeight * 0.15}`);
    // header slider
    $('.header_main .header_bg img').prop('src',`${$(`.slider .swiper-container .swiper-wrapper .swiper-slide:nth-child(1)`).data('image')}`);
    var swiper = new Swiper('.header_main .swiper-container', {
      pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
    $(`.swiper-pagination-total`).text(`0` + swiper.slides.length);
    $(`.swiper-pagination-current`).text(`01`);
    swiper.on('slideChange', function () {
      $(`.swiper-pagination-current`).text(`0${swiper.activeIndex + 1}`);
      $(`.swiper-pagination-total`).text(`0` + swiper.slides.length);
      $('.header_main .header_bg').fadeOut();
      setTimeout(function () {
        $('.header_main .header_bg img').prop('src',`${$(`.slider .swiper-container .swiper-wrapper .swiper-slide:nth-child(${swiper.activeIndex+1})`).data('image')}`);
        $('.header_main .header_bg').css('background-image',`url(${$(`.slider .swiper-container .swiper-wrapper .swiper-slide:nth-child(${swiper.activeIndex+1})`).data('image')})`);
        $('.header_main .header_bg').fadeIn();
      }, 400);
    })
    $("#point-content .point_text").dotdotdot({
      height: 60
    });
    $(".blog_text").dotdotdot({
      height: 100
    });
    if (document.documentElement.clientHeight > 700) {
      $(".slide-text").dotdotdot({
        height: 200
      });
      $(".slide-title").dotdotdot({
        height: 250
      });
      console.log('sdfsdf')
    } else {
      $(".slide-text").dotdotdot({
        height: 100
      });
      $(".slide-title").dotdotdot({
        height: 250
      });
    }
  }
  //tag for projects
  var tags = document.querySelectorAll('.tag .tag_item');
  for (let i = 0; i < tags.length; ++i) {
    if (!tags[i].textContent) {
      tags[i].style.opacity = 0;
    }
  }

  $('.proj-link-item').on('click', function () {
    $(this).prop("href", $('.swiper-slide').eq(swiper.activeIndex).data('link'));
  })
  $(".swipe").click(function () {
    var elementClick = $(this).attr("href");
    var destination = $(elementClick).offset().top;
    $('html').animate({scrollTop: destination}, 1100);
    return false;
  });
  //input extand-toggle
  $('input.form-control').on('click', function () {
    $(this).toggleClass('show')
  });

  //scrolling fadeIn blocks
  $("main.page-index section").each(function (indx, element) {
    var waypoints = $(element).waypoint({
      handler: function (direction) {
        $(element).addClass('animated')
      },
      offset: '70%'
    })
  });
  if ($('.blog_item_page').length || $('.projects_item_page').length) {
    //gallery
    var galleryTop = new Swiper('.gallery-top', {
      navigation: {
        nextEl: '#gallery-next',
        prevEl: '#gallery-prev',
      },
      loop: true,
      loopedSlides: 4
    });
    var galleryThumbs = new Swiper('.gallery-thumbs', {
      spaceBetween: 30,
      slidesPerView: 3,
      centeredSlides: true,
      touchRatio: 0.2,
      slideToClickedSlide: true,
      loop: true,
      loopedSlides: 4
    });
    galleryTop.controller.control = galleryThumbs;
    galleryThumbs.controller.control = galleryTop;
  }
  if ($('.projects_item_page').length) {
    // review slider
    var swiper2 = new Swiper('.reviews-swiper', {
      slidesPerView: 3,
      spaceBetween: 30,
      navigation: {
        nextEl: '#reviews-next',
        prevEl: '#reviews-prev',
      },
    });
  }
  $(".LiquidFill").imgLiquid({
    fill: true,
    horizontalAlign: "center",
    verticalAlign: "center"
  });

  $("#navbarDropdown").on('mouseenter  touchstart', function () {
    $(this).dropdown('toggle');
  });
  if($('.page-map-index').length) {
    $(function () {
      $( ".point" ).draggable({ containment: "parent" });
    });
    $('.map').on('click', function (e) {
      if (!e.target.classList.contains('point')) {
        var x = e.offsetX==undefined?e.layerX:e.offsetX;
        var y = e.offsetY==undefined?e.layerY:e.offsetY;
        let index = $('.point').length;
        let point = `<div class='point' data-placement="top" data-popover-content="#point-content" data-toggle="popover" data-html="true" data-y="${y}" data-x="${x}"></div>`;
        $(point).appendTo(this).css({top:$(point).data("y"), left:$(point).data("x")});
        $(function () {
          $(".point").draggable({ containment: "parent" });
        });
        $( ".point" ).on( "dragstop", function( event, ui ) {
          $(this).data("x", $(this).position().left);
          $(this).data("y", $(this).position().top);
        });
      }
      console.log(e.target);
    });
    $(window).on('resize', function(){
      if (document.documentElement.clientWidth >= 1183) {
        $('.point').each(function (index, elem) {
          $(this).css({left: $(this).data("x"), top: $(this).data("y")});
        });
      } else if (document.documentElement.clientWidth < 1183 && document.documentElement.clientWidth >= 975) {
        $('.point').each(function (index, elem) {
          $(this).css({left: $(this).data("x")*0.83, top: $(this).data("y")*0.83});
        });
      } else if (document.documentElement.clientWidth < 975 && document.documentElement.clientWidth >= 751) {
        $('.point').each(function (index, elem) {
          $(this).css({left: $(this).data("x")*0.62, top: $(this).data("y")*0.62});
        });
      } else if (document.documentElement.clientWidth < 751 && document.documentElement.clientWidth >= 559) {
        $('.point').each(function (index, elem) {
          $(this).css({left: $(this).data("x")*0.45, top: $(this).data("y")*0.45});
        });
      } else if (document.documentElement.clientWidth < 559) {
        $('.point').each(function (index, elem) {
          $(this).css({left: $(this).data("x")*0.32, top: $(this).data("y")*0.32});
        });
      }
    });
  }


  if($('.projects_page').length) {
    $('.more_item').on('click', function () {
      $('form').toggleClass('state-more');
    });
    $('.item').on('click', function () {
      $('form').addClass('state-check');
      let dataSet = $(this).data('set');
      $(`*[data-set="${dataSet}"]`).toggleClass('hide');
      $(`#ch${dataSet}`).prop('value') ? $(`#ch${dataSet}`).prop('value', '') : $(`#ch${dataSet}`).prop('value', dataSet);
      let i = 0;
      $('.filters_block_check .item').each(function (index, value) {
        if (!$(this).hasClass('hide')) {
          ++i;
        }
      });
      if (i == 0) {
        $('form').removeClass('state-check');
      }
    });
    $('.clear_filters').on('click', function () {
      $('form').toggleClass('state-check');
      $('.filters_block_check .item').addClass('hide');
      $('.filters_block .item').removeClass('hide');
      $('.filters_block input').prop('value', '');
      let selects = document.querySelectorAll('option');
      for (let i = 0; i < selects.length; i++) {
        select2[i].selected = false;
      }
      $('.clear_filters').removeClass('show')
    });
    $('#inlineFormCustomSelect1, #inlineFormCustomSelect2').styler();
    $('.jq-selectbox').on('click', function () {
      let select1 = document.querySelectorAll('#inlineFormCustomSelect1 option');
      let select2 = document.querySelectorAll('#inlineFormCustomSelect2 option');
      for (let i = 1; i < select1.length; i++) {
        if (select1[i].selected) {
          $('.clear_filters').addClass('show')
          $('#inlineFormCustomSelect1').siblings('.jq-selectbox__select').addClass('active');
        }
      }
      for (let i = 1; i < select2.length; i++) {
        if (select2[i].selected) {
          $('.clear_filters').addClass('show')
          $('#inlineFormCustomSelect2').siblings('.jq-selectbox__select').addClass('active');
        }
      }
    })
  }


  if($('.projects_item_page').length) {
    $("a.swiper-slide").fancybox();
  }
  var adresse = '';
  function init() {
    var myMap;
    var myGeocoder = ymaps.geocode($("#map_route").data('adresse'));
    myGeocoder.then(
      function (res) {
        adresse = res.geoObjects.get(0).geometry.getCoordinates();
        if ($('.contacts_route').length) {
          myMap = new ymaps.Map('map_route', { center: adresse, controls: ['zoomControl','routePanelControl'], zoom: 17  }, { searchControlProvider: 'yandex#search' });
          var control = myMap.controls.get('routePanelControl');
          control.routePanel.state.set({ from: '', to: $("#map_route").data('adresse') });
        } else {
          myMap = new ymaps.Map('map_route', { center: adresse, controls: ['zoomControl'], zoom: 17 }, { searchControlProvider: 'yandex#search' });
          var myGeoObject = new ymaps.GeoObject({geoometry: {type: "Point", coordinates: adresse} });
          myMap.geoObjects.add(new ymaps.Placemark(adresse, {}, {preset: 'islands#redDotIcon'}));
        }
      },
      function (err) {
        alert('Ошибка');
      }
    );
  }
  ymaps.ready(init);
});